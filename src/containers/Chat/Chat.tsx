import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import fakePosts from '../../data';
import './Chat.css';
import Spinner from '../../components/Spinner/Spinner';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import ChatPosts from '../../components/ChatPosts/ChatPosts';
import ChatEditor from '../../components/ChatEditor/ChatEditor';
import { TypeUser, TypePost } from '../../models/types';
import ChatEditorModal from '../../components/ChatEditorModal/ChatEditorModal';

type Props = {
  currentUser: TypeUser;
};

function Chat({ currentUser }: Props) {
  const [posts, setPosts] = useState([] as TypePost[]);
  const [editablePost, setEditablePost] = useState({} as TypePost);
  const [isLoading, setIsLoading] = useState(false);

  const loadPosts = () => {
    setIsLoading(true);
    setTimeout(() => {
      setPosts(fakePosts);
      setIsLoading(false);
    }, 1000);
  };

  useEffect(() => {
    if (currentUser.id) loadPosts();
  }, [currentUser]);

  const countUsers = () => {
    const users = new Set(posts.map((post) => post.user));
    return users.size;
  };

  const addPost = (value: string) => {
    setPosts([
      ...posts,
      {
        id: uuidv4(),
        text: value,
        user: currentUser.name,
        userId: currentUser.id,
        avatar: '',
        createdAt: new Date().toJSON(),
        editedAt: '',
      },
    ]);
  };

  const deletePost = (post: TypePost): void => {
    const id = posts.findIndex((pst) => pst.id === post.id);
    setPosts([...posts.slice(0, id), ...posts.slice(id + 1)]);
  };

  const editPost = (post: TypePost): void => {
    setEditablePost(post);
  };

  const updatePost = (post: TypePost): void => {
    const id = posts.findIndex((pst) => pst.id === post.id);
    setPosts([...posts.slice(0, id), post, ...posts.slice(id + 1)]);
    closeModal();
  };

  const likePost = (post: TypePost): void => {
    updatePost({
      ...post,
      isLikes: !post.isLikes,
    });
  };

  const closeModal = (): void => {
    setEditablePost({} as TypePost);
  };

  return (
    <div className="chat ui container">
      <ChatHeader
        chatName={'Sort By'}
        countUsers={countUsers()}
        countPosts={posts.length}
        dateLastPost={posts.length ? posts[posts.length - 1].createdAt : ''}
      />
      <ChatPosts
        posts={posts}
        currentUser={currentUser}
        editPost={editPost}
        deletePost={deletePost}
        likePost={likePost}
      />
      <ChatEditor onSubmit={addPost} />
      {editablePost.id && (
        <ChatEditorModal post={editablePost} updatePost={updatePost} close={closeModal} />
      )}
      {isLoading && <Spinner />}
    </div>
  );
}

Chat.propTypes = {
  currentUser: PropTypes.object.isRequired,
};

export default Chat;
