import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import './Page.css';
import PageHeader from '../../components/PageHeader/PageHeader';
import PageFooter from '../../components/PageFooter/PageFooter';
import Chat from '../Chat/Chat';
import LoginModal from '../../components/LoginModal/LoginModal';
import { TypeUser } from '../../models/types';

function Page() {
  const [currentUser, setCurrentUser] = useState({} as TypeUser);

  const login = (userName: string) => {
    setCurrentUser({
      id: uuidv4(),
      name: userName,
    });
  };

  return (
    <div className="page ui">
      <PageHeader userName={currentUser.name} />
      <Chat currentUser={currentUser} />
      <PageFooter />
      {!currentUser.id && <LoginModal onSubmit={login} isShow={!currentUser.id} />}
    </div>
  );
}

export default Page;
