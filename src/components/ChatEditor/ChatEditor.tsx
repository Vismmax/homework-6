import React, { FormEvent, useRef } from 'react';
import PropTypes from 'prop-types';
import './ChatEditor.css';

type Props = {
  onSubmit: (value: string) => void;
};

function ChatEditor({ onSubmit }: Props) {
  const textarea = useRef(null as any);

  const onSubmitHandler = (ev: FormEvent) => {
    onSubmit(textarea.current.value);
    textarea.current.value = '';
    ev.preventDefault();
  };

  return (
    <form className="chat-editor ui form" onSubmit={onSubmitHandler}>
      <div className="textarea-wrap">
        <textarea rows={2} ref={textarea} />
      </div>
      <div className="send-wrap">
        <button className="ui button huge" type="submit">
          <i className="paper plane icon"></i>
          Send
        </button>
      </div>
    </form>
  );
}

ChatEditor.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default ChatEditor;
