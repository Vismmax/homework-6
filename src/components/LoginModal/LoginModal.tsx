import React, { FormEvent, useState } from 'react';
import PropTypes from 'prop-types';
import './LoginModal.css';

type Props = {
  isShow: boolean;
  onSubmit: (value: string) => void;
};

function LoginModal({ onSubmit }: Props) {
  const [userName, setUserName] = useState('');

  const onSubmitHandler = (ev: FormEvent) => {
    onSubmit(userName);
    ev.preventDefault();
  };

  return (
    <div className="login-modal ui dimmer modals visible active">
      <div className="ui tiny modal visible active">
        <div className="header">Login</div>
        <div className="content">
          <form className="ui form big" onSubmit={onSubmitHandler}>
            <div className="login-fields fields">
              <div className="field-name field">
                <label>User Name</label>
                <input
                  placeholder="User Name"
                  type="text"
                  value={userName}
                  onChange={(e) => setUserName(e.target.value)}
                />
              </div>
              <div className="field">
                <button className="ui button big" type="submit">
                  Login
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

LoginModal.propTypes = {
  isShow: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default LoginModal;
