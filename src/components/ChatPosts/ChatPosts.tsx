import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './ChatPosts.css';
import ChatPost from '../ChatPost/ChatPost';
import { TypePost, TypeUser } from '../../models/types';
import ChatDivider from '../ChatDivider/ChatDivider';

type Props = {
  posts: TypePost[];
  currentUser: TypeUser;
  editPost: (post: TypePost) => void;
  deletePost: (post: TypePost) => void;
  likePost: (post: TypePost) => void;
};

function ChatPosts({ posts, currentUser, editPost, deletePost, likePost }: Props) {
  const getPostElements = () => {
    const elements: JSX.Element[] = [];
    let day = 0;

    posts.forEach((post) => {
      const newDay = new Date(post.createdAt).getDate();
      if (newDay !== day) {
        elements.push(
          <ChatDivider
            key={newDay}
            date={moment(post.createdAt).calendar({
              sameDay: '[Today]',
              nextDay: '[Tomorrow]',
              nextWeek: 'dddd',
              lastDay: '[Yesterday]',
              lastWeek: '[Last] dddd',
              sameElse: 'DD/MM/YYYY',
            })}
          />,
        );
        day = newDay;
      }
      elements.push(
        <ChatPost
          key={post.id}
          post={post}
          right={post.userId === currentUser.id}
          editPost={editPost}
          deletePost={deletePost}
          likePost={likePost}
        />,
      );
    });

    return elements;
  };

  return <div className="chat-posts ui segment feed">{getPostElements()}</div>;
}

ChatPosts.propTypes = {
  posts: PropTypes.array.isRequired,
  currentUser: PropTypes.object.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
};

export default ChatPosts;
