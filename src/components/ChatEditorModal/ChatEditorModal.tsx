import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './ChatEditorModal.css';
import { TypePost } from '../../models/types';

type Props = {
  post: TypePost;
  updatePost: (post: TypePost) => void;
  close: () => void;
};

function ChatEditorModal({ post, updatePost, close }: Props) {
  const [text, setText] = useState(post.text);

  return (
    <div className="login-modal ui dimmer modals visible active">
      <div className="ui tiny modal visible active">
        <div className="header">Edit Post</div>
        <div className="content">
          <div className="ui form">
            <textarea rows={3} value={text} onChange={(e) => setText(e.target.value)} />
          </div>
        </div>
        <div className="actions">
          <button className="ui button" onClick={() => close()}>
            Cancel
          </button>
          <button className="ui button primary" onClick={() => updatePost({ ...post, text })}>
            Update
          </button>
        </div>
      </div>
    </div>
  );
}

ChatEditorModal.propTypes = {
  post: PropTypes.object.isRequired,
  updatePost: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
};

export default ChatEditorModal;
