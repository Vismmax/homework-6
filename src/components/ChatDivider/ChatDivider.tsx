import React from 'react';
import PropTypes from 'prop-types';
import './ChatDivider.css';

type Props = {
  date: string;
};

function ChatDivider({ date }: Props) {
  return (
    <div className="ui horizontal divider">
      <div className="ui label">{date}</div>
    </div>
  );
}

ChatDivider.propTypes = {
  date: PropTypes.string.isRequired,
};

export default ChatDivider;
