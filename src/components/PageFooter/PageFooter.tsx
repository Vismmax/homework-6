import React from 'react';
import './PageFooter.css';

function PageFooter() {
  return (
    <div className="page-footer ui text menu">
      <div className="item">Copyright</div>
    </div>
  );
}

export default PageFooter;
