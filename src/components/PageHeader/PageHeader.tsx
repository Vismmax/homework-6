import React from 'react';
import PropTypes from 'prop-types';
import './PageHeader.css';
import logo from '../../img/logo.svg';

type Props = {
  userName: string | undefined;
};

function PageHeader({ userName }: Props) {
  return (
    <div className="page-header ui text menu">
      <div className="item">
        <img src={logo} alt="Logo" />
      </div>
      <div className="header item right">{userName}</div>
    </div>
  );
}

PageHeader.propTypes = {
  userName: PropTypes.string.isRequired,
};

export default PageHeader;
