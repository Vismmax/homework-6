export type TypePost = {
  id: string;
  text: string;
  user: string;
  userId: string;
  avatar: string;
  createdAt: string;
  editedAt: string;
  isLikes?: boolean;
};

export type TypeUser = {
  id: string;
  name: string;
};
